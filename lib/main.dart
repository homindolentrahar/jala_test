import 'package:flutter/material.dart';
import 'package:jala_test/core/presentation/theme/app_theme.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/route/app_routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await AppModule.inject();

  runApp(const JalaTestApp());
}

class JalaTestApp extends StatelessWidget {
  const JalaTestApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.lightTheme,
      routerConfig: AppRoutes.router,
    );
  }
}
