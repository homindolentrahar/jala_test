import 'package:get_it/get_it.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/harga_udang/data/repository/harga_udang_repository.dart';
import 'package:jala_test/features/harga_udang/data/repository/region_repository.dart';
import 'package:jala_test/features/harga_udang/detail/data/repository/detail_harga_repository.dart';
import 'package:jala_test/features/harga_udang/detail/domain/repository/i_detail_harga_udang_repository.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_harga_udang_repository.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_region_repository.dart';
import 'package:jala_test/features/kabar_udang/data/repository/kabar_udang_repository.dart';
import 'package:jala_test/features/kabar_udang/domain/repository/i_kabar_udang_repository.dart';
import 'package:jala_test/features/penyakit_udang/data/repository/penyakit_udang_repository.dart';
import 'package:jala_test/features/penyakit_udang/domain/repository/i_penyakit_udang_repository.dart';

final injector = GetIt.instance;

abstract class AppModule {
  static Future<void> inject() async {
    injector.registerSingleton<ApiClient>(ApiClient());

    injector.registerLazySingleton<IRegionRepository>(
      () => RegionRepository(injector.get<ApiClient>()),
    );
    injector.registerLazySingleton<IHargaUdangRepository>(
      () => HargaUdangRepository(injector.get<ApiClient>()),
    );
    injector.registerLazySingleton<IDetailHargaUdangRepository>(
      () => DetailHargaUdangRepository(injector.get<ApiClient>()),
    );
    injector.registerLazySingleton<IKabarUdangRepository>(
      () => KabarUdangRepository(injector.get<ApiClient>()),
    );
    injector.registerLazySingleton<IPenyakitUdangRepository>(
      () => PenyakitUdangRepository(injector.get<ApiClient>()),
    );
  }
}
