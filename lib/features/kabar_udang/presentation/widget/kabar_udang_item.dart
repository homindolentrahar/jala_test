import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';
import 'package:jala_test/core/presentation/widget/shimmer_widget.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/verified_chip.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';
import 'package:jala_test/util/extension/date_time_ext.dart';
import 'package:jala_test/util/helper/share_helper.dart';

class KabarUdangItem extends StatelessWidget {
  final KabarUdangDto data;
  final ValueChanged<int?> onPressed;

  const KabarUdangItem({
    super.key,
    required this.data,
    required this.onPressed,
  });

  static Widget loading(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).colorScheme.outline),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ShimmerWidget(
            radius: const BorderRadius.only(
              topLeft: Radius.circular(8),
              topRight: Radius.circular(8),
            ),
            width: MediaQuery.of(context).size.width,
            height: 160,
          ),
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ShimmerWidget(width: double.infinity, height: 18),
                SizedBox(height: 4),
                ShimmerWidget(width: double.infinity, height: 12),
                SizedBox(height: 2),
                ShimmerWidget(width: 80, height: 12),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ShimmerWidget(width: 72, height: 12),
                    ShimmerWidget(
                      width: 24,
                      height: 24,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPressed(data.id),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).colorScheme.outline),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
              ),
              child: CachedNetworkImage(
                imageUrl: "https://app.jala.tech/storage/${data.image}",
                width: MediaQuery.of(context).size.width,
                height: 160,
                fit: BoxFit.cover,
                errorWidget: (context, url, error) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: 160,
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.outline,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                      ),
                    ),
                  );
                },
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.surface,
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.title ?? "-",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Theme.of(context)
                        .textTheme
                        .headlineLarge
                        ?.copyWith(fontWeight: FontWeight.w900),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    data.excerpt ?? "-",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: AppColors.textSubtitle),
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(360),
                              child: CachedNetworkImage(
                                imageUrl:
                                    "https://app.jala.tech/storage/${data.author?.avatar}",
                                width: 32,
                                height: 32,
                                fit: BoxFit.cover,
                                errorWidget: (context, url, error) => Container(
                                  width: 32,
                                  height: 32,
                                  decoration: BoxDecoration(
                                    color:
                                        Theme.of(context).colorScheme.outline,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 8),
                            Text(
                              data.author?.name ?? "-",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(color: AppColors.textSubtitle),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 8),
                      VerifiedChip(isVerified: data.author?.buyer ?? false),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        data.createdAt?.formatDate() ?? "-",
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(color: AppColors.textSubtitle),
                      ),
                      GestureDetector(
                        onTap: () {
                          ShareHelper.shareUrl(
                            "https://app.jala.tech/posts/${data.id}",
                          );
                        },
                        child: SvgPicture.asset(
                          "assets/icons/ic_share.svg",
                          width: 24,
                          height: 24,
                          color: Theme.of(context).colorScheme.onSurface,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
