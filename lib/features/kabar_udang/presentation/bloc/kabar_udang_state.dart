import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';

part 'kabar_udang_state.freezed.dart';

@freezed
class KabarUdangState with _$KabarUdangState {
  const factory KabarUdangState({
    @Default(BaseStatus.initial) BaseStatus status,
    @Default([]) List<KabarUdangDto> data,
    @Default("") String error,
    @Default(1) int page,
    @Default(15) int perPage,
    @Default(false) bool hasMorePage,
  }) = _KabarUdangState;
}
