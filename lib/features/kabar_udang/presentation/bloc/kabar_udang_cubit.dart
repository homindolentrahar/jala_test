import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/bloc/cubit_base_mixin.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';
import 'package:jala_test/features/kabar_udang/domain/repository/i_kabar_udang_repository.dart';
import 'package:jala_test/features/kabar_udang/presentation/bloc/kabar_udang_state.dart';

class KabarUdangCubit extends Cubit<KabarUdangState> with CubitBaseMixin {
  KabarUdangCubit() : super(const KabarUdangState()) {
    loadData();
  }

  final IKabarUdangRepository repository =
      injector.get<IKabarUdangRepository>();

  void refreshLoad() {
    emit(state.copyWith(hasMorePage: false));

    loadData();
  }

  void loadNextData() {
    loadData(page: state.page + 1);
  }

  Future<void> loadData({int page = 1, int perPage = 15}) async {
    emit(
      state.copyWith(
        status: BaseStatus.loading,
        page: page,
        perPage: perPage,
      ),
    );

    final result = await repository.getKabarUdang(page: page, perPage: perPage);

    result.fold(
      (error) => emit(
        state.copyWith(status: BaseStatus.error, error: error),
      ),
      (data) {
        final tempList = List<KabarUdangDto>.from(state.data);
        if (state.page == 1) {
          tempList.clear();
        }

        tempList.addAll(data);

        emit(
          state.copyWith(
            status: BaseStatus.success,
            data: tempList,
            error: "",
            hasMorePage: !(data.length < state.perPage),
          ),
        );
      },
    );

    refreshCompleted();
  }
}
