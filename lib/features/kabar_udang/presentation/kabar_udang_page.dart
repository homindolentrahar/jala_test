import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:jala_test/core/presentation/widget/state_reactive_widget.dart';
import 'package:jala_test/features/kabar_udang/presentation/bloc/kabar_udang_cubit.dart';
import 'package:jala_test/features/kabar_udang/presentation/bloc/kabar_udang_state.dart';
import 'package:jala_test/features/kabar_udang/presentation/widget/kabar_udang_item.dart';
import 'package:jala_test/route/app_routes.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class KabarUdangPage extends StatefulWidget {
  const KabarUdangPage._();

  static Widget getPage() => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => KabarUdangCubit()),
        ],
        child: const KabarUdangPage._(),
      );

  @override
  State<KabarUdangPage> createState() => _KabarUdangPageState();
}

class _KabarUdangPageState extends State<KabarUdangPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return BlocBuilder<KabarUdangCubit, KabarUdangState>(builder: (ctx, state) {
      return SmartRefresher(
        controller: context.read<KabarUdangCubit>().refreshController,
        enablePullDown: true,
        enablePullUp: state.hasMorePage,
        onLoading: context.read<KabarUdangCubit>().loadNextData,
        onRefresh: context.read<KabarUdangCubit>().refreshLoad,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Kabar terbaru",
                  style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                ),
                const SizedBox(height: 16),
                StateReactiveWidget(
                  status: state.status,
                  hasMorePage: state.hasMorePage,
                  errorMessage: state.error,
                  loadingWidget: ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 10,
                    separatorBuilder: (_, index) => const SizedBox(height: 12),
                    itemBuilder: (_, index) => KabarUdangItem.loading(context),
                  ),
                  child: ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.data.length,
                    separatorBuilder: (_, index) => const SizedBox(height: 12),
                    itemBuilder: (_, index) => KabarUdangItem(
                      data: state.data[index],
                      onPressed: (id) {
                        context.pushNamed(
                          RoutePaths.kabarUdang,
                          pathParameters: {
                            'id': id.toString(),
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
