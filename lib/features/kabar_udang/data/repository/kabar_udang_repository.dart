import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';
import 'package:jala_test/features/kabar_udang/domain/repository/i_kabar_udang_repository.dart';

class KabarUdangRepository implements IKabarUdangRepository {
  final ApiClient client;

  KabarUdangRepository(this.client);

  @override
  Future<Either<String, List<KabarUdangDto>>> getKabarUdang({
    int perPage = 15,
    int page = 1,
    String withKeyword = "author",
  }) async {
    try {
      final result = await client.requestList<KabarUdangDto>(
        endpoint: "/api/posts",
        method: RestMethod.get,
        queryParams: {
          'per_page': perPage,
          'page': page,
          'with': withKeyword,
        },
      );

      log("Result: ${result.toJson()}");

      return right(result.data ?? []);
    } catch (e) {
      log("Error: ${e.toString()}");
      return left(e.toString());
    }
  }
}
