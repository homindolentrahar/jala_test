import 'package:jala_test/features/kabar_udang/data/dto/author_dto.dart';

class KabarUdangDto {
  final int? id;
  final int? authorId;
  final int? categoryId;
  final String? image;
  final String? status;
  final bool? featured;
  final dynamic advertisement;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? title;
  final String? seoTitle;
  final String? excerpt;
  final String? body;
  final String? slug;
  final String? metaDescription;
  final String? metaKeywords;
  final AuthorDto? author;

  KabarUdangDto({
    this.id,
    this.authorId,
    this.categoryId,
    this.image,
    this.status,
    this.featured,
    this.advertisement,
    this.createdAt,
    this.updatedAt,
    this.title,
    this.seoTitle,
    this.excerpt,
    this.body,
    this.slug,
    this.metaDescription,
    this.metaKeywords,
    this.author,
  });

  factory KabarUdangDto.fromJson(Map<String, dynamic> json) => KabarUdangDto(
        id: json["id"],
        authorId: json["author_id"],
        categoryId: json["category_id"],
        image: json["image"],
        status: json["status"],
        featured: json["featured"],
        advertisement: json["advertisement"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        title: json["title"],
        seoTitle: json["seo_title"],
        excerpt: json["excerpt"],
        body: json["body"],
        slug: json["slug"],
        metaDescription: json["meta_description"],
        metaKeywords: json["meta_keywords"],
        author:
            json["author"] == null ? null : AuthorDto.fromJson(json["author"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "author_id": authorId,
        "category_id": categoryId,
        "image": image,
        "status": status,
        "featured": featured,
        "advertisement": advertisement,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "title": title,
        "seo_title": seoTitle,
        "excerpt": excerpt,
        "body": body,
        "slug": slug,
        "meta_description": metaDescription,
        "meta_keywords": metaKeywords,
        "author": author?.toJson(),
      };
}
