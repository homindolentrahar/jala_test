import 'package:jala_test/core/data/remote/dio/settings_dto.dart';
import 'package:jala_test/core/data/remote/dio/state_dto.dart';

class AuthorDto {
  final int? id;
  final int? roleId;
  final String? name;
  final String? email;
  final String? avatar;
  final bool? emailVerified;
  final int? subscriptionTypeId;
  final SettingsDto? settings;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final dynamic regionId;
  final dynamic address;
  final DateTime? lastLoginAt;
  final dynamic deactivated;
  final DateTime? expiredAt;
  final String? phone;
  final bool? phoneVerified;
  final dynamic gender;
  final String? occupation;
  final dynamic idNumber;
  final dynamic idScan;
  final dynamic tinNumber;
  final dynamic tinScan;
  final DateTime? birthdate;
  final dynamic company;
  final dynamic companyAddress;
  final dynamic position;
  final dynamic monthlyIncome;
  final dynamic incomeSource;
  final dynamic buyer;
  final String? phoneCountry;
  final dynamic country;
  final String? interest;
  final dynamic unsubscribeEmailAt;
  final dynamic freshchatRestoreId;
  final dynamic allowCreateClient;
  final dynamic allowCreateToken;
  final List<String>? interests;
  final StateDto? state;
  final dynamic familyCardNumber;
  final dynamic familyCardScan;
  final dynamic telegramId;
  final dynamic genderName;
  final DateTime? expiredDate;
  final String? expiredTime;
  final DateTime? upcomingBirthdate;

  AuthorDto({
    this.id,
    this.roleId,
    this.name,
    this.email,
    this.avatar,
    this.emailVerified,
    this.subscriptionTypeId,
    this.settings,
    this.createdAt,
    this.updatedAt,
    this.regionId,
    this.address,
    this.lastLoginAt,
    this.deactivated,
    this.expiredAt,
    this.phone,
    this.phoneVerified,
    this.gender,
    this.occupation,
    this.idNumber,
    this.idScan,
    this.tinNumber,
    this.tinScan,
    this.birthdate,
    this.company,
    this.companyAddress,
    this.position,
    this.monthlyIncome,
    this.incomeSource,
    this.buyer,
    this.phoneCountry,
    this.country,
    this.interest,
    this.unsubscribeEmailAt,
    this.freshchatRestoreId,
    this.allowCreateClient,
    this.allowCreateToken,
    this.interests,
    this.state,
    this.familyCardNumber,
    this.familyCardScan,
    this.telegramId,
    this.genderName,
    this.expiredDate,
    this.expiredTime,
    this.upcomingBirthdate,
  });

  factory AuthorDto.fromJson(Map<String, dynamic> json) => AuthorDto(
        id: json["id"],
        roleId: json["role_id"],
        name: json["name"],
        email: json["email"],
        avatar: json["avatar"],
        emailVerified: json["email_verified"],
        subscriptionTypeId: json["subscription_type_id"],
        settings: json["settings"] == null
            ? null
            : SettingsDto.fromJson(json["settings"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        regionId: json["region_id"],
        address: json["address"],
        lastLoginAt: json["last_login_at"] == null
            ? null
            : DateTime.parse(json["last_login_at"]),
        deactivated: json["deactivated"],
        expiredAt: json["expired_at"] == null
            ? null
            : DateTime.parse(json["expired_at"]),
        phone: json["phone"],
        phoneVerified: json["phone_verified"],
        gender: json["gender"],
        occupation: json["occupation"],
        idNumber: json["id_number"],
        idScan: json["id_scan"],
        tinNumber: json["tin_number"],
        tinScan: json["tin_scan"],
        birthdate: json["birthdate"] == null
            ? null
            : DateTime.parse(json["birthdate"]),
        company: json["company"],
        companyAddress: json["company_address"],
        position: json["position"],
        monthlyIncome: json["monthly_income"],
        incomeSource: json["income_source"],
        buyer: json["buyer"],
        phoneCountry: json["phone_country"],
        country: json["country"],
        interest: json["interest"],
        unsubscribeEmailAt: json["unsubscribe_email_at"],
        freshchatRestoreId: json["freshchat_restore_id"],
        allowCreateClient: json["allow_create_client"],
        allowCreateToken: json["allow_create_token"],
        interests: json["interests"] == null
            ? []
            : List<String>.from(json["interests"].map((x) => x)),
        state: json["state"] == null ? null : StateDto.fromJson(json["state"]),
        familyCardNumber: json["family_card_number"],
        familyCardScan: json["family_card_scan"],
        telegramId: json["telegram_id"],
        genderName: json["gender_name"],
        expiredDate: json["expired_date"] == null
            ? null
            : DateTime.parse(json["expired_date"]),
        expiredTime: json["expired_time"],
        upcomingBirthdate: json["upcoming_birthdate"] == null
            ? null
            : DateTime.parse(json["upcoming_birthdate"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "role_id": roleId,
        "name": name,
        "email": email,
        "avatar": avatar,
        "email_verified": emailVerified,
        "subscription_type_id": subscriptionTypeId,
        "settings": settings?.toJson(),
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "region_id": regionId,
        "address": address,
        "last_login_at": lastLoginAt?.toIso8601String(),
        "deactivated": deactivated,
        "expired_at": expiredAt?.toIso8601String(),
        "phone": phone,
        "phone_verified": phoneVerified,
        "gender": gender,
        "occupation": occupation,
        "id_number": idNumber,
        "id_scan": idScan,
        "tin_number": tinNumber,
        "tin_scan": tinScan,
        "birthdate": birthdate?.toIso8601String(),
        "company": company,
        "company_address": companyAddress,
        "position": position,
        "monthly_income": monthlyIncome,
        "income_source": incomeSource,
        "buyer": buyer,
        "phone_country": phoneCountry,
        "country": country,
        "interest": interest,
        "unsubscribe_email_at": unsubscribeEmailAt,
        "freshchat_restore_id": freshchatRestoreId,
        "allow_create_client": allowCreateClient,
        "allow_create_token": allowCreateToken,
        "interests": interests == null
            ? []
            : List<dynamic>.from(interests?.map((x) => x) ?? []),
        "state": state?.toJson(),
        "family_card_number": familyCardNumber,
        "family_card_scan": familyCardScan,
        "telegram_id": telegramId,
        "gender_name": genderName,
        "expired_date": expiredDate?.toIso8601String(),
        "expired_time": expiredTime,
        "upcoming_birthdate": upcomingBirthdate?.toIso8601String(),
      };
}
