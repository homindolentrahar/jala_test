import 'package:dartz/dartz.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';

abstract interface class IKabarUdangRepository {
  Future<Either<String, List<KabarUdangDto>>> getKabarUdang({
    int perPage = 15,
    int page = 1,
    String withKeyword = "author",
  });
}
