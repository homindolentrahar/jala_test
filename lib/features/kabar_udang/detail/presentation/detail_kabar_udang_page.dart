import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/kabar_udang/detail/presentation/bloc/detail_kabar_udang_cubit.dart';
import 'package:jala_test/features/kabar_udang/detail/presentation/bloc/detail_kabar_udang_state.dart';
import 'package:jala_test/util/helper/share_helper.dart';

class DetailKabarUdangPage extends StatelessWidget {
  final int? id;

  const DetailKabarUdangPage._(this.id);

  static Widget getPage(int? id) => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => DetailKabarUdangCubit()),
        ],
        child: DetailKabarUdangPage._(id),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Kabar Udang"),
        actions: [
          IconButton(
            onPressed: () {
              ShareHelper.shareUrl(
                "https://app.jala.tech/posts/$id",
              );
            },
            icon: const Icon(
              Icons.share_outlined,
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: BlocBuilder<DetailKabarUdangCubit, DetailKabarUdangState>(
          builder: (ctx, state) {
            return Stack(
              children: [
                InAppWebView(
                  initialUrlRequest: URLRequest(
                    url: Uri.parse("https://app.jala.tech/web_view/posts/$id"),
                  ),
                  onWebViewCreated: (controller) {
                    context
                        .read<DetailKabarUdangCubit>()
                        .inAppWebViewController = controller;

                    context.read<DetailKabarUdangCubit>().loading();
                  },
                  initialOptions: InAppWebViewGroupOptions(
                    android: AndroidInAppWebViewOptions(),
                  ),
                  onLoadStart: (controller, url) {
                    context.read<DetailKabarUdangCubit>().loading();
                  },
                  onLoadStop: (controller, url) {
                    context.read<DetailKabarUdangCubit>().success();
                  },
                  onLoadHttpError: (controller, url, statusCode, error) {
                    context.read<DetailKabarUdangCubit>().error(error);
                  },
                ),
                if (state.status == BaseStatus.loading) ...[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Theme.of(context).colorScheme.surface,
                    child: const Center(child: CircularProgressIndicator()),
                  ),
                ],
              ],
            );
          },
        ),
      ),
    );
  }
}
