import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';

part 'detail_kabar_udang_state.freezed.dart';

@freezed
class DetailKabarUdangState with _$DetailKabarUdangState {
  const factory DetailKabarUdangState({
    @Default(BaseStatus.initial) BaseStatus status,
    @Default("") String error,
  }) = _DetailKabarUdangState;
}
