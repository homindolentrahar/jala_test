import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/bloc/cubit_base_mixin.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/features/penyakit_udang/data/dto/penyakit_udang_dto.dart';
import 'package:jala_test/features/penyakit_udang/domain/repository/i_penyakit_udang_repository.dart';
import 'package:jala_test/features/penyakit_udang/presentation/bloc/penyakit_udang_state.dart';

class PenyakitUdangCubit extends Cubit<PenyakitUdangState> with CubitBaseMixin {
  PenyakitUdangCubit() : super(const PenyakitUdangState()) {
    loadData();
  }

  final IPenyakitUdangRepository repository =
      injector.get<IPenyakitUdangRepository>();

  void refreshData() {
    emit(state.copyWith(hasMorePage: false));

    loadData();
  }

  void loadNextData() {
    loadData(page: state.page + 1);
  }

  Future<void> loadData({int page = 1, int perPage = 15}) async {
    emit(
      state.copyWith(
        status: BaseStatus.loading,
        page: page,
        perPage: perPage,
      ),
    );

    final result = await repository.getPenyakitUdang(
      page: page,
      perPage: perPage,
    );

    result.fold(
      (error) => emit(
        state.copyWith(status: BaseStatus.error, error: error),
      ),
      (data) {
        final tempList = List<PenyakitUdangDto>.from(state.data);
        if (state.page == 1) {
          tempList.clear();
        }

        tempList.addAll(data);

        emit(
          state.copyWith(
            status: BaseStatus.success,
            data: tempList,
            error: "",
            hasMorePage: !(data.length < state.perPage),
          ),
        );
      },
    );

    refreshCompleted();
  }
}
