import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/penyakit_udang/data/dto/penyakit_udang_dto.dart';

part 'penyakit_udang_state.freezed.dart';

@freezed
class PenyakitUdangState with _$PenyakitUdangState {
  const factory PenyakitUdangState({
    @Default(BaseStatus.initial) BaseStatus status,
    @Default([]) List<PenyakitUdangDto> data,
    @Default("") String error,
    @Default(1) int page,
    @Default(15) int perPage,
    @Default(false) bool hasMorePage,
  }) = _PenyakitUdangState;
}
