import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:jala_test/core/presentation/widget/state_reactive_widget.dart';
import 'package:jala_test/features/penyakit_udang/presentation/bloc/penyakit_udang_cubit.dart';
import 'package:jala_test/features/penyakit_udang/presentation/bloc/penyakit_udang_state.dart';
import 'package:jala_test/features/penyakit_udang/presentation/widget/penyakit_udang_item.dart';
import 'package:jala_test/route/app_routes.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PenyakitUdangPage extends StatefulWidget {
  const PenyakitUdangPage._();

  static Widget getPage() => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => PenyakitUdangCubit()),
        ],
        child: const PenyakitUdangPage._(),
      );

  @override
  State<PenyakitUdangPage> createState() => _PenyakitUdangPageState();
}

class _PenyakitUdangPageState extends State<PenyakitUdangPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return BlocBuilder<PenyakitUdangCubit, PenyakitUdangState>(
        builder: (ctx, state) {
      return SmartRefresher(
        controller: context.read<PenyakitUdangCubit>().refreshController,
        enablePullDown: true,
        enablePullUp: state.hasMorePage,
        onLoading: context.read<PenyakitUdangCubit>().loadNextData,
        onRefresh: context.read<PenyakitUdangCubit>().refreshData,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Daftar Penyakit",
                  style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                ),
                const SizedBox(height: 16),
                StateReactiveWidget(
                  status: state.status,
                  errorMessage: state.error,
                  hasMorePage: state.hasMorePage,
                  loadingWidget: ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 10,
                    separatorBuilder: (_, index) => const SizedBox(height: 12),
                    itemBuilder: (_, index) =>
                        PenyakitUdangItem.loading(context),
                  ),
                  child: ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.data.length,
                    separatorBuilder: (_, index) => const SizedBox(height: 12),
                    itemBuilder: (_, index) => PenyakitUdangItem(
                      data: state.data[index],
                      onPressed: (id) {
                        context.pushNamed(
                          RoutePaths.penyakitUdang,
                          pathParameters: {
                            'id': id.toString(),
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
