import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/penyakit_udang/detail/presentation/bloc/detail_penyakit_udang_cubit.dart';
import 'package:jala_test/features/penyakit_udang/detail/presentation/bloc/detail_penyakit_udang_state.dart';
import 'package:jala_test/util/helper/share_helper.dart';

class DetailPenyakitUdangPage extends StatelessWidget {
  final int? id;

  const DetailPenyakitUdangPage._(this.id);

  static Widget getPage(int? id) => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => DetailPenyakitUdangCubit()),
        ],
        child: DetailPenyakitUdangPage._(id),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Info Penyakit"),
        actions: [
          IconButton(
            onPressed: () {
              ShareHelper.shareUrl(
                "https://app.jala.tech/diseases/$id",
              );
            },
            icon: const Icon(
              Icons.share_outlined,
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: BlocBuilder<DetailPenyakitUdangCubit, DetailPenyakitUdangState>(
          builder: (ctx, state) {
            return Stack(
              children: [
                InAppWebView(
                  initialUrlRequest: URLRequest(
                    url: Uri.parse(
                        "https://app.jala.tech/web_view/diseases/$id"),
                  ),
                  onWebViewCreated: (controller) {
                    context
                        .read<DetailPenyakitUdangCubit>()
                        .inAppWebViewController = controller;

                    context.read<DetailPenyakitUdangCubit>().loading();
                  },
                  initialOptions: InAppWebViewGroupOptions(
                    android: AndroidInAppWebViewOptions(),
                  ),
                  onLoadStart: (controller, url) {
                    context.read<DetailPenyakitUdangCubit>().loading();
                  },
                  onLoadStop: (controller, url) {
                    context.read<DetailPenyakitUdangCubit>().success();
                  },
                  onLoadHttpError: (controller, url, statusCode, error) {
                    context.read<DetailPenyakitUdangCubit>().error(error);
                  },
                ),
                if (state.status == BaseStatus.loading) ...[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Theme.of(context).colorScheme.surface,
                    child: const Center(child: CircularProgressIndicator()),
                  ),
                ],
              ],
            );
          },
        ),
      ),
    );
  }
}
