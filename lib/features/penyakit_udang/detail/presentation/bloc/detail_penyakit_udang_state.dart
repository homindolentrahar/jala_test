import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';

part 'detail_penyakit_udang_state.freezed.dart';

@freezed
class DetailPenyakitUdangState with _$DetailPenyakitUdangState {
  const factory DetailPenyakitUdangState({
    @Default(BaseStatus.initial) BaseStatus status,
    @Default("") String error,
  }) = _DetailPenyakitUdangState;
}
