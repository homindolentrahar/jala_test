import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/penyakit_udang/detail/presentation/bloc/detail_penyakit_udang_state.dart';

class DetailPenyakitUdangCubit extends Cubit<DetailPenyakitUdangState> {
  DetailPenyakitUdangCubit() : super(const DetailPenyakitUdangState());

  InAppWebViewController? inAppWebViewController;

  void loading() {
    emit(state.copyWith(status: BaseStatus.loading));
  }

  void success() {
    emit(state.copyWith(status: BaseStatus.success));
  }

  void error(String message) {
    emit(state.copyWith(status: BaseStatus.error, error: message));
  }
}
