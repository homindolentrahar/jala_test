import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/penyakit_udang/data/dto/penyakit_udang_dto.dart';
import 'package:jala_test/features/penyakit_udang/domain/repository/i_penyakit_udang_repository.dart';

class PenyakitUdangRepository implements IPenyakitUdangRepository {
  final ApiClient client;

  PenyakitUdangRepository(this.client);

  @override
  Future<Either<String, List<PenyakitUdangDto>>> getPenyakitUdang({
    int page = 1,
    int perPage = 15,
  }) async {
    try {
      final result = await client.requestList<PenyakitUdangDto>(
        endpoint: "/api/diseases",
        method: RestMethod.get,
        queryParams: {
          'per_page': perPage,
          'page': page,
        },
      );

      log("Result: ${result.toJson()}");

      return right(result.data ?? []);
    } catch (e) {
      log("Error: ${e.toString()}");
      return left(e.toString());
    }
  }
}
