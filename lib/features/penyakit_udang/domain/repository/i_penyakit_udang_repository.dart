import 'package:dartz/dartz.dart';
import 'package:jala_test/features/penyakit_udang/data/dto/penyakit_udang_dto.dart';

abstract interface class IPenyakitUdangRepository {
  Future<Either<String, List<PenyakitUdangDto>>> getPenyakitUdang({
    int page = 1,
    int perPage = 15,
  });
}
