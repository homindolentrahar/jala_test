import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/presentation/widget/colored_tab_bar.dart';
import 'package:jala_test/features/main/presentation/cubit/main_cubit.dart';

class MainPage extends StatefulWidget {
  const MainPage._();

  @override
  State<MainPage> createState() => _MainPageState();

  static Widget getPage() => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => MainCubit()),
        ],
        child: const MainPage._(),
      );
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  late MainCubit _mainCubit;
  late TabController _tabController;

  @override
  void initState() {
    super.initState();

    _mainCubit = context.read<MainCubit>();
    _tabController = TabController(
      length: _mainCubit.tabViews.length,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Jala Media"),
        bottom: ColoredTabBar(
          controller: _tabController,
          tabs: _mainCubit.tabViews.map((tab) => Text(tab.title)).toList(),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: _mainCubit.tabViews.map((e) => e.view).toList(),
      ),
    );
  }
}
