import 'package:freezed_annotation/freezed_annotation.dart';

part 'main_state.freezed.dart';

@freezed
class MainState with _$MainState {
  const factory MainState({
    @Default(0) int tabIndex,
    @Default(20) int size,
  }) = _MainState;
}
