import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/features/harga_udang/presentation/harga_udang_page.dart';
import 'package:jala_test/features/kabar_udang/presentation/kabar_udang_page.dart';
import 'package:jala_test/features/main/presentation/cubit/main_state.dart';
import 'package:jala_test/features/penyakit_udang/presentation/penyakit_udang_page.dart';

class MainTabView {
  final int index;
  final String title;
  final Widget view;

  MainTabView({
    required this.index,
    required this.title,
    required this.view,
  });
}

class MainCubit extends Cubit<MainState> {
  MainCubit() : super(const MainState());

  List<MainTabView> tabViews = [
    MainTabView(index: 0, title: "Harga Udang", view: HargaUdangPage.getPage()),
    MainTabView(index: 1, title: "Kabar Udang", view: KabarUdangPage.getPage()),
    MainTabView(index: 2, title: "Penyakit", view: PenyakitUdangPage.getPage()),
  ];

  void onTabChanged(int tabIndex) {
    emit(state.copyWith(tabIndex: tabIndex));
  }

  void onSizeFilterChanged(int size) {
    emit(state.copyWith(size: size));
  }
}
