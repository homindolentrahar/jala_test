import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';

part 'harga_udang_filter_location_state.freezed.dart';

@freezed
class HargaUdangFilterLocationState with _$HargaUdangFilterLocationState {
  const factory HargaUdangFilterLocationState({
    @Default([]) List<RegionDto> data,
    @Default("") String error,
    @Default(0) int selectedRegionId,
    @Default(BaseStatus.initial) BaseStatus status,
    @Default("") String query,
    @Default(1) int page,
    @Default(10) int perPage,
    @Default(false) bool hasMorePage,
  }) = _HargaUdangFilterLocationState;
}
