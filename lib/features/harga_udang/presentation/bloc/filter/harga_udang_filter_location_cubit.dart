import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/bloc/cubit_base_mixin.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_region_repository.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/filter/harga_udang_filter_location_state.dart';

class HargaUdangFilterLocationCubit extends Cubit<HargaUdangFilterLocationState>
    with CubitBaseMixin {
  HargaUdangFilterLocationCubit()
      : super(const HargaUdangFilterLocationState()) {
    loadData();
  }

  final IRegionRepository repository = injector.get<IRegionRepository>();
  final TextEditingController searchController = TextEditingController();
  Timer? _debounce;

  void refreshData() {
    emit(state.copyWith(hasMorePage: false));
    loadData(page: 1, search: state.query);
  }

  void loadNextData() {
    loadData(
      page: state.page + 1,
      search: state.query,
    );
  }

  void searchRegion(String query) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      loadData(search: query, page: 1);
    });
  }

  void resetSearch() {
    searchController.clear();
    loadData();
  }

  Future<void> loadData({
    int page = 1,
    int perPage = 15,
    String search = "",
  }) async {
    emit(
      state.copyWith(
        status: BaseStatus.loading,
        page: page,
        perPage: perPage,
        query: search,
      ),
    );

    final result = await repository.getAllRegions(
      page: page,
      perPage: perPage,
      search: search,
    );

    result.fold(
      (error) => emit(
        state.copyWith(status: BaseStatus.error, error: error),
      ),
      (data) {
        final tempList = List<RegionDto>.from(state.data);

        if (state.page == 1) {
          tempList.clear();
        }

        tempList.addAll(data);

        emit(
          state.copyWith(
            status: BaseStatus.success,
            data: tempList,
            error: "",
            hasMorePage: !(data.length < state.perPage),
          ),
        );
      },
    );

    refreshCompleted();
  }

  @override
  Future<void> close() {
    _debounce?.cancel();

    return super.close();
  }
}
