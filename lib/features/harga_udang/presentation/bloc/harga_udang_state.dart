import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';

part 'harga_udang_state.freezed.dart';

@freezed
class HargaUdangState with _$HargaUdangState {
  const factory HargaUdangState({
    @Default(20) int filterSize,
    RegionDto? filterLocation,
    @Default(BaseStatus.initial) BaseStatus status,
    @Default([]) List<HargaUdangDto> data,
    @Default("") String errorMessage,
    @Default(1) int page,
    @Default(10) int perPage,
    @Default(false) bool hasMorePage,
  }) = _HargaUdangState;
}
