import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/bloc/cubit_base_mixin.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_harga_udang_repository.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/harga_udang_state.dart';

class FiturLainnyaBanner {
  final String title;
  final String banner;

  FiturLainnyaBanner(this.title, this.banner);
}

class HargaUdangCubit extends Cubit<HargaUdangState> with CubitBaseMixin {
  HargaUdangCubit() : super(const HargaUdangState()) {
    loadData();
  }

  final IHargaUdangRepository repository =
      injector.get<IHargaUdangRepository>();

  final List<FiturLainnyaBanner> fiturLainnyaBanner = [
    FiturLainnyaBanner("Ask Jali", "assets/images/ask_jali.png"),
    FiturLainnyaBanner("Quiz Jala", "assets/images/quiz_jala.png"),
  ];

  void refreshData() {
    emit(state.copyWith(hasMorePage: false));

    loadNextData();
  }

  void loadNextData() {
    loadData(page: state.page + 1);
  }

  Future<void> loadData({
    int page = 1,
    int perPage = 15,
    String withKeyword = "region, creator",
  }) async {
    emit(
      state.copyWith(
        status: BaseStatus.loading,
        page: page,
        perPage: perPage,
      ),
    );

    final result = await repository.getHargaUdang(
      page: page,
      perPage: perPage,
      regionId: int.tryParse(state.filterLocation?.id ?? "0"),
      withKeyword: withKeyword,
    );

    result.fold(
      (error) => emit(
        state.copyWith(status: BaseStatus.error, errorMessage: error),
      ),
      (data) {
        final tempList = List<HargaUdangDto>.from(state.data);
        if (state.page == 1) {
          tempList.clear();
        }

        tempList.addAll(data);

        emit(
          state.copyWith(
            status: BaseStatus.success,
            data: tempList,
            errorMessage: "",
            hasMorePage: !(data.length < state.perPage),
          ),
        );
      },
    );

    refreshCompleted();
  }

  void onSizeChanged(int size) async {
    emit(state.copyWith(filterSize: size));
  }

  void onLocationChanged(RegionDto location) async {
    emit(state.copyWith(filterLocation: location, hasMorePage: false));

    loadData();
  }
}
