import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class VerifiedChip extends StatelessWidget {
  final bool isVerified;

  const VerifiedChip({
    super.key,
    this.isVerified = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 6,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(360),
        color: isVerified
            ? Theme.of(context).colorScheme.secondaryContainer
            : Theme.of(context).colorScheme.outline,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (isVerified) ...[
            SvgPicture.asset(
              "assets/icons/ic_verified.svg",
              width: 16,
              height: 16,
            ),
            const SizedBox(width: 4),
          ],
          Text(
            isVerified ? "Terverifikasi" : "belum terverifikasi",
            style: Theme.of(context).textTheme.bodySmall,
          ),
        ],
      ),
    );
  }
}
