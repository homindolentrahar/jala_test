import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jala_test/core/presentation/widget/primary_button.dart';
import 'package:jala_test/core/presentation/widget/shimmer_widget.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/verified_chip.dart';
import 'package:jala_test/util/extension/date_time_ext.dart';
import 'package:jala_test/util/extension/double_ext.dart';
import 'package:jala_test/util/extension/string_ext.dart';

class HargaUdangItem extends StatelessWidget {
  final int size;
  final HargaUdangDto data;
  final ValueChanged<HargaUdangDto> onItemPressed;

  const HargaUdangItem({
    super.key,
    required this.size,
    required this.data,
    required this.onItemPressed,
  });

  static Widget loading(BuildContext context) => Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
            color: Theme.of(context).colorScheme.outline,
            width: 1,
          ),
          boxShadow: [
            BoxShadow(
              color: const Color(0xFF000000).withOpacity(0.04),
              offset: const Offset(0, 4),
              blurRadius: 8,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ShimmerWidget(
                        width: 32,
                        height: 32,
                        radius: BorderRadius.circular(360),
                      ),
                      const SizedBox(width: 8),
                      const Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ShimmerWidget(width: 48, height: 12),
                          SizedBox(height: 2),
                          ShimmerWidget(width: 96, height: 14),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 12),
                ShimmerWidget(
                  width: 80,
                  height: 20,
                  radius: BorderRadius.circular(360),
                ),
              ],
            ),
            const SizedBox(height: 8),
            const ShimmerWidget(width: 56, height: 12),
            const SizedBox(height: 4),
            const ShimmerWidget(width: 64, height: 12),
            const SizedBox(height: 2),
            const ShimmerWidget(width: 96, height: 18),
            const SizedBox(height: 8),
            const Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ShimmerWidget(width: 24, height: 12),
                      SizedBox(height: 2),
                      ShimmerWidget(width: 56, height: 22),
                    ],
                  ),
                ),
                SizedBox(width: 16),
                ShimmerWidget(width: 112, height: 32),
              ],
            ),
          ],
        ),
      );

  int? get pricePerSize {
    final jsonData = data.toJson();
    final price = jsonData["size_$size"];

    return price;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: Theme.of(context).colorScheme.outline,
          width: 1,
        ),
        boxShadow: [
          BoxShadow(
            color: const Color(0xFF000000).withOpacity(0.04),
            offset: const Offset(0, 4),
            blurRadius: 8,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(360),
                      child: CachedNetworkImage(
                        imageUrl:
                            "https://app.jala.tech/storage/${data.creator?.avatar}",
                        width: 32,
                        height: 32,
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) => Container(
                          width: 32,
                          height: 32,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.outline,
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Supplier",
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onSecondaryContainer),
                        ),
                        Text(
                          data.creator?.name ?? "-",
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 12),
              VerifiedChip(isVerified: data.creator?.buyer ?? false),
            ],
          ),
          const SizedBox(height: 8),
          Text(
            data.date?.formatDate() ?? "-",
            style: Theme.of(context).textTheme.bodySmall?.copyWith(
                  color: Theme.of(context).colorScheme.onSecondaryContainer,
                ),
          ),
          const SizedBox(height: 4),
          Text(
            data.region?.provinceName?.capitalize ?? "-",
            style: Theme.of(context).textTheme.bodySmall,
          ),
          Text(
            data.region?.name?.capitalize ?? "-",
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const SizedBox(height: 4),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "size $size",
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                    Text(
                      pricePerSize?.toDouble().formatCurrency() ?? "Rp. -",
                      style: Theme.of(context).textTheme.displayLarge,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              PrimaryButton(
                isUppercase: true,
                title: "Lihat Detail",
                onPressed: () {
                  onItemPressed(data);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
