import 'package:flutter/material.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/harga_udang_cubit.dart';

class FiturLainnyaScrollBanner extends StatelessWidget {
  final List<FiturLainnyaBanner> options;
  final ValueChanged<FiturLainnyaBanner> onBannerSelected;

  const FiturLainnyaScrollBanner({
    super.key,
    required this.options,
    required this.onBannerSelected,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Wrap(
        direction: Axis.horizontal,
        spacing: 16,
        children: options
            .map(
              (banner) => GestureDetector(
                onTap: () => onBannerSelected(banner),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.asset(
                    banner.banner,
                    fit: BoxFit.cover,
                    width: 270,
                    height: 100,
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
