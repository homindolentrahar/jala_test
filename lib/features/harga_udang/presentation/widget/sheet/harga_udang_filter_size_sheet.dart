import 'package:flutter/material.dart';

class HargaUdangFilterSizeSheet extends StatelessWidget {
  final ValueChanged<int> onSizePressed;

  const HargaUdangFilterSizeSheet({super.key, required this.onSizePressed});

  List<int> get sizeFilters {
    int size = 10;
    List<int> filters = [];

    while (size < 200) {
      size += 10;
      filters.add(size);
    }

    return filters;
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (ctx) => Column(
        children: [
          Container(
            padding: const EdgeInsets.all(16).copyWith(bottom: 12),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                  width: 1.5,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Size",
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Text(
                    "Tutup",
                    style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: sizeFilters.length,
              itemBuilder: (_, index) => InkWell(
                onTap: () {
                  Navigator.pop(context);

                  onSizePressed(sizeFilters[index]);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 12,
                  ),
                  child: Text(
                    sizeFilters[index].toString(),
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
