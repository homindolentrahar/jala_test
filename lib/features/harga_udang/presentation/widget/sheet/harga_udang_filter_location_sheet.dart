import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/widget/primary_search_field.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/filter/harga_udang_filter_location_cubit.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/filter/harga_udang_filter_location_state.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HargaUdangFilterLocationSheet extends StatelessWidget {
  final RegionDto? initialRegion;
  final ValueChanged<RegionDto> onRegionChanged;

  const HargaUdangFilterLocationSheet._({
    this.initialRegion,
    required this.onRegionChanged,
  });

  static Widget getPage({
    RegionDto? initialRegion,
    required ValueChanged<RegionDto> onLocationChanged,
  }) =>
      MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => HargaUdangFilterLocationCubit()),
        ],
        child: HargaUdangFilterLocationSheet._(
          initialRegion: initialRegion,
          onRegionChanged: onLocationChanged,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      backgroundColor: Theme.of(context).colorScheme.surface,
      builder: (ctx) => Column(
        children: [
          Container(
            padding: const EdgeInsets.all(16).copyWith(bottom: 12),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                  width: 1.5,
                ),
              ),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Kota/Kabupaten",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                      child: Text(
                        "Tutup",
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium
                            ?.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                PrimarySearchField(
                  controller: ctx
                      .read<HargaUdangFilterLocationCubit>()
                      .searchController,
                  hint: "Cari",
                  onLocationSearch: (value) {
                    if (value != null) {
                      ctx
                          .read<HargaUdangFilterLocationCubit>()
                          .searchRegion(value);
                    }
                  },
                  onLocationReset: () {
                    ctx.read<HargaUdangFilterLocationCubit>().resetSearch();
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: BlocBuilder<HargaUdangFilterLocationCubit,
                HargaUdangFilterLocationState>(
              builder: (ctxt, state) {
                if (state.status == BaseStatus.success || state.hasMorePage) {
                  return SmartRefresher(
                    controller: ctx
                        .read<HargaUdangFilterLocationCubit>()
                        .refreshController,
                    enablePullDown: true,
                    enablePullUp: state.hasMorePage,
                    onLoading:
                        ctx.read<HargaUdangFilterLocationCubit>().loadNextData,
                    onRefresh:
                        ctx.read<HargaUdangFilterLocationCubit>().refreshData,
                    child: ListView.builder(
                      itemCount: state.data.length,
                      itemBuilder: (_, index) => InkWell(
                        onTap: () {
                          Navigator.pop(context);

                          onRegionChanged(state.data[index]);
                        },
                        child: Container(
                          color: initialRegion?.id == state.data[index].id
                              ? Theme.of(context).primaryColor.withOpacity(0.15)
                              : Colors.transparent,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 12,
                          ),
                          child: Text(
                            state.data[index].name ?? "-",
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state.status == BaseStatus.loading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                return const Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
