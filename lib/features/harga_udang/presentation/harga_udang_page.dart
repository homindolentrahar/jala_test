import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:jala_test/core/presentation/widget/state_reactive_widget.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/harga_udang_cubit.dart';
import 'package:jala_test/features/harga_udang/presentation/bloc/harga_udang_state.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/fitur_lainnya_scroll_banner.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/harga_udang_bottom_filter.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/harga_udang_item.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/sheet/harga_udang_filter_location_sheet.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/sheet/harga_udang_filter_size_sheet.dart';
import 'package:jala_test/route/app_routes.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HargaUdangPage extends StatefulWidget {
  const HargaUdangPage._();

  static Widget getPage() => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => HargaUdangCubit()),
        ],
        child: const HargaUdangPage._(),
      );

  @override
  State<HargaUdangPage> createState() => _HargaUdangPageState();
}

class _HargaUdangPageState extends State<HargaUdangPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Stack(
      children: [
        BlocBuilder<HargaUdangCubit, HargaUdangState>(
          builder: (ctx, state) {
            return SmartRefresher(
              controller: context.read<HargaUdangCubit>().refreshController,
              enablePullDown: true,
              enablePullUp: state.hasMorePage,
              onRefresh: context.read<HargaUdangCubit>().refreshData,
              onLoading: context.read<HargaUdangCubit>().loadNextData,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.surface,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Text(
                              "Coba Fitur Lainnya",
                              style: Theme.of(context).textTheme.headlineMedium,
                            ),
                          ),
                          const SizedBox(height: 16),
                          FiturLainnyaScrollBanner(
                            options:
                                ctx.read<HargaUdangCubit>().fiturLainnyaBanner,
                            onBannerSelected: (value) {
                              showAdaptiveDialog(
                                context: context,
                                builder: (ctx) => AlertDialog(
                                  title: const Text("Coba Fitur Lainnya"),
                                  titleTextStyle:
                                      Theme.of(context).textTheme.headlineLarge,
                                  backgroundColor:
                                      Theme.of(context).colorScheme.surface,
                                  surfaceTintColor:
                                      Theme.of(context).colorScheme.surface,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  content: RichText(
                                    text: TextSpan(
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                      children: [
                                        TextSpan(
                                          text: value.title,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                fontWeight: FontWeight.bold,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .primary,
                                              ),
                                        ),
                                        const TextSpan(text: " clicked!"),
                                      ],
                                    ),
                                  ),
                                  actionsAlignment: MainAxisAlignment.center,
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        context.pop();
                                      },
                                      child: Text(
                                        "OK",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineMedium
                                            ?.copyWith(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 2),
                    Container(
                      padding: const EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.surface,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Harga terbaru",
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onPrimaryContainer,
                                ),
                          ),
                          const SizedBox(height: 8),
                          StateReactiveWidget(
                            status: state.status,
                            errorMessage: state.errorMessage,
                            hasMorePage: state.hasMorePage,
                            loadingWidget: ListView.separated(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              separatorBuilder: (_, index) =>
                                  const SizedBox(height: 8),
                              itemBuilder: (_, index) =>
                                  HargaUdangItem.loading(context),
                            ),
                            child: ListView.separated(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: state.data.length,
                              separatorBuilder: (_, index) =>
                                  const SizedBox(height: 8),
                              itemBuilder: (_, index) => HargaUdangItem(
                                size: state.filterSize,
                                data: state.data[index],
                                onItemPressed: (value) {
                                  context.pushNamed(
                                    RoutePaths.hargaUdang,
                                    pathParameters: {
                                      'id': value.id?.toString() ?? "",
                                    },
                                    extra: value.regionId,
                                  );
                                },
                              ),
                            ),
                          ),
                          if (!state.hasMorePage) ...[
                            const SizedBox(height: 56),
                          ],
                        ],
                      ),
                    ),
                    // const SizedBox(height: 64),
                  ],
                ),
              ),
            );
          },
        ),
        Positioned(
          bottom: 8,
          left: 12,
          right: 12,
          child: BlocBuilder<HargaUdangCubit, HargaUdangState>(
            builder: (ctx, state) {
              return HargaUdangBottomFilter(
                size: state.filterSize,
                region: state.filterLocation,
                onSizePressed: () {
                  showModalBottomSheet(
                    context: context,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16),
                      ),
                    ),
                    builder: (_) => HargaUdangFilterSizeSheet(
                      onSizePressed: (amount) {
                        ctx.read<HargaUdangCubit>().onSizeChanged(amount);
                      },
                    ),
                  );
                },
                onLocationPressed: () {
                  showModalBottomSheet(
                    context: context,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16),
                      ),
                    ),
                    builder: (_) => HargaUdangFilterLocationSheet.getPage(
                      initialRegion: state.filterLocation,
                      onLocationChanged: (location) {
                        ctx.read<HargaUdangCubit>().onLocationChanged(location);
                      },
                    ),
                  );
                },
              );
            },
          ),
        )
      ],
    );
  }
}
