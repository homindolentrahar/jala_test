import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_region_repository.dart';

class RegionRepository implements IRegionRepository {
  final ApiClient client;

  RegionRepository(this.client);

  @override
  Future<Either<String, List<RegionDto>>> getAllRegions({
    String has = "shrimp_prices",
    String? search,
    int page = 1,
    int perPage = 15,
  }) async {
    try {
      final result = await client.requestList<RegionDto>(
        endpoint: "/api/regions",
        method: RestMethod.get,
        queryParams: {
          'has': has,
          'search': search,
          'page': page,
          'per_page': perPage,
        },
      );

      log("Result: ${result.toJson()}");

      return right(result.data ?? []);
    } catch (e) {
      log("Error: ${e.toString()}");
      return left(e.toString());
    }
  }
}
