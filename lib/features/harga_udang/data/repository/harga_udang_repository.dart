import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/domain/repository/i_harga_udang_repository.dart';

class HargaUdangRepository implements IHargaUdangRepository {
  final ApiClient client;

  HargaUdangRepository(this.client);

  @override
  Future<Either<String, List<HargaUdangDto>>> getHargaUdang({
    int perPage = 15,
    int page = 1,
    String? withKeyword,
    int? regionId,
  }) async {
    try {
      final result = await client.requestList<HargaUdangDto>(
        endpoint: "/api/shrimp_prices",
        method: RestMethod.get,
        queryParams: {
          'per_page': perPage,
          'page': page,
          'with': withKeyword,
          'region_id': regionId,
        },
      );

      log("Result: ${result.toJson()}");

      return right(result.data ?? []);
    } catch (e) {
      log("Error: ${e.toString()}");
      return left(e.toString());
    }
  }
}
