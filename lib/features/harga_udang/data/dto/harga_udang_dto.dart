import 'package:jala_test/features/harga_udang/data/dto/creator_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';

class HargaUdangDto {
  final int? id;
  final int? speciesId;
  final DateTime? date;
  final dynamic size20;
  final int? size30;
  final int? size40;
  final int? size50;
  final int? size60;
  final int? size70;
  final int? size80;
  final int? size90;
  final int? size100;
  final dynamic size110;
  final dynamic size120;
  final dynamic size130;
  final dynamic size140;
  final dynamic size150;
  final dynamic size160;
  final dynamic size170;
  final dynamic size180;
  final dynamic size190;
  final int? size200;
  final dynamic remark;
  final int? createdBy;
  final int? updatedBy;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? regionId;
  final String? contact;
  final String? countryId;
  final String? currencyId;
  final dynamic private;
  final int? week;
  final String? dateRegionFullName;
  final String? provinceId;
  final String? regencyId;
  final String? districtId;
  final String? villageId;
  final RegionDto? region;
  final CreatorDto? creator;

  HargaUdangDto({
    this.id,
    this.speciesId,
    this.date,
    this.size20,
    this.size30,
    this.size40,
    this.size50,
    this.size60,
    this.size70,
    this.size80,
    this.size90,
    this.size100,
    this.size110,
    this.size120,
    this.size130,
    this.size140,
    this.size150,
    this.size160,
    this.size170,
    this.size180,
    this.size190,
    this.size200,
    this.remark,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
    this.regionId,
    this.contact,
    this.countryId,
    this.currencyId,
    this.private,
    this.week,
    this.dateRegionFullName,
    this.provinceId,
    this.regencyId,
    this.districtId,
    this.villageId,
    this.region,
    this.creator,
  });

  factory HargaUdangDto.fromJson(Map<String, dynamic> json) => HargaUdangDto(
        id: json["id"],
        speciesId: json["species_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        size20: json["size_20"],
        size30: json["size_30"],
        size40: json["size_40"],
        size50: json["size_50"],
        size60: json["size_60"],
        size70: json["size_70"],
        size80: json["size_80"],
        size90: json["size_90"],
        size100: json["size_100"],
        size110: json["size_110"],
        size120: json["size_120"],
        size130: json["size_130"],
        size140: json["size_140"],
        size150: json["size_150"],
        size160: json["size_160"],
        size170: json["size_170"],
        size180: json["size_180"],
        size190: json["size_190"],
        size200: json["size_200"],
        remark: json["remark"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        regionId: json["region_id"],
        contact: json["contact"],
        countryId: json["country_id"],
        currencyId: json["currency_id"],
        private: json["private"],
        week: json["week"],
        dateRegionFullName: json["date_region_full_name"],
        provinceId: json["province_id"],
        regencyId: json["regency_id"],
        districtId: json["district_id"],
        villageId: json["village_id"],
        region:
            json["region"] == null ? null : RegionDto.fromJson(json["region"]),
        creator: json["creator"] == null
            ? null
            : CreatorDto.fromJson(json["creator"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "species_id": speciesId,
        "date": date?.toIso8601String(),
        "size_20": size20,
        "size_30": size30,
        "size_40": size40,
        "size_50": size50,
        "size_60": size60,
        "size_70": size70,
        "size_80": size80,
        "size_90": size90,
        "size_100": size100,
        "size_110": size110,
        "size_120": size120,
        "size_130": size130,
        "size_140": size140,
        "size_150": size150,
        "size_160": size160,
        "size_170": size170,
        "size_180": size180,
        "size_190": size190,
        "size_200": size200,
        "remark": remark,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "region_id": regionId,
        "contact": contact,
        "country_id": countryId,
        "currency_id": currencyId,
        "private": private,
        "week": week,
        "date_region_full_name": dateRegionFullName,
        "province_id": provinceId,
        "regency_id": regencyId,
        "district_id": districtId,
        "village_id": villageId,
        "region": region?.toJson(),
        "creator": creator?.toJson(),
      };
}
