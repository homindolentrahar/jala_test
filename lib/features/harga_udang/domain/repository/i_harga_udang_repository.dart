import 'package:dartz/dartz.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';

abstract interface class IHargaUdangRepository {
  Future<Either<String, List<HargaUdangDto>>> getHargaUdang({
    int perPage = 15,
    int page = 1,
    String? withKeyword,
    int? regionId,
  });
}
