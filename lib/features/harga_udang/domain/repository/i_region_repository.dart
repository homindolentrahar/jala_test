import 'package:dartz/dartz.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';

abstract interface class IRegionRepository {
  Future<Either<String, List<RegionDto>>> getAllRegions({
    String has = "shrimp_prices",
    String? search,
    int page = 1,
    int perPage = 2,
  });
}
