import 'package:dartz/dartz.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';

abstract interface class IDetailHargaUdangRepository {
  Future<Either<String, HargaUdangDto>> getDetailHargaUdang({
    String? withKeyword,
    required int id,
    required int regionId,
  });
}
