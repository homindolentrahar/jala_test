import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:jala_test/core/data/remote/api_client.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/detail/domain/repository/i_detail_harga_udang_repository.dart';

class DetailHargaUdangRepository implements IDetailHargaUdangRepository {
  final ApiClient client;

  DetailHargaUdangRepository(this.client);

  @override
  Future<Either<String, HargaUdangDto>> getDetailHargaUdang({
    String? withKeyword,
    required int id,
    required int regionId,
  }) async {
    try {
      final result = await client.requestObject<HargaUdangDto>(
        endpoint: "/api/shrimp_prices/$id",
        method: RestMethod.get,
        queryParams: {
          'with': withKeyword ?? "region,creator",
          'region_id': regionId,
        },
      );

      log("Result: ${result.toJson()}");

      return right(result.data!);
    } catch (e) {
      log("Error: ${e.toString()}");
      return left(e.toString());
    }
  }
}
