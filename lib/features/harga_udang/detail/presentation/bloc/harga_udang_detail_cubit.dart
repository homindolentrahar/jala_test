import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/presentation/bloc/cubit_base_mixin.dart';
import 'package:jala_test/di/app_module.dart';
import 'package:jala_test/features/harga_udang/detail/domain/repository/i_detail_harga_udang_repository.dart';
import 'package:jala_test/features/harga_udang/detail/presentation/bloc/harga_udang_detail_state.dart';

class HargaUdangDetailCubit extends Cubit<HargaUdangDetailState>
    with CubitBaseMixin {
  HargaUdangDetailCubit({int? id, int? regionId})
      : super(const HargaUdangDetailState.initial()) {
    loadData(id: id, regionId: regionId);
  }

  final IDetailHargaUdangRepository repository =
      injector.get<IDetailHargaUdangRepository>();

  Future<void> loadData({int? id, int? regionId}) async {
    emit(const HargaUdangDetailState.loading());

    final result = await repository.getDetailHargaUdang(
      id: id ?? 0,
      regionId: regionId ?? 0,
    );

    result.fold(
      (error) => emit(
        HargaUdangDetailState.error(error),
      ),
      (data) {
        emit(
          HargaUdangDetailState.success(data),
        );
      },
    );

    refreshCompleted();
  }
}
