import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';

part 'harga_udang_detail_state.freezed.dart';

@freezed
class HargaUdangDetailState with _$HargaUdangDetailState {
  const factory HargaUdangDetailState.initial() = _Initial;

  const factory HargaUdangDetailState.loading() = _Loading;

  const factory HargaUdangDetailState.success(HargaUdangDto data) = _Success;

  const factory HargaUdangDetailState.error(String error) = _Error;
}
