import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';
import 'package:jala_test/core/presentation/widget/primary_button.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/detail/presentation/bloc/harga_udang_detail_cubit.dart';
import 'package:jala_test/features/harga_udang/detail/presentation/bloc/harga_udang_detail_state.dart';
import 'package:jala_test/features/harga_udang/presentation/widget/verified_chip.dart';
import 'package:jala_test/util/extension/date_time_ext.dart';
import 'package:jala_test/util/extension/double_ext.dart';
import 'package:jala_test/util/extension/string_ext.dart';
import 'package:jala_test/util/helper/share_helper.dart';
import 'package:jala_test/util/helper/url_launcher_helper.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HargaUdangDetailPage extends StatelessWidget {
  final int? id;
  final int? regionId;

  const HargaUdangDetailPage._(this.id, this.regionId);

  static Widget getPage({
    required String id,
    required String regionId,
  }) =>
      MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => HargaUdangDetailCubit(
              id: int.tryParse(id),
              regionId: int.tryParse(regionId),
            ),
          ),
        ],
        child: HargaUdangDetailPage._(
          int.tryParse(id),
          int.tryParse(regionId),
        ),
      );

  Map<int, int?> getPrices(HargaUdangDto? data) => {
        20: data?.size20,
        30: data?.size30,
        40: data?.size40,
        50: data?.size50,
        60: data?.size60,
        70: data?.size70,
        80: data?.size80,
        90: data?.size90,
        100: data?.size100,
        110: data?.size110,
        120: data?.size120,
        130: data?.size130,
        140: data?.size140,
        150: data?.size150,
        160: data?.size160,
        170: data?.size170,
        180: data?.size180,
        190: data?.size190,
        200: data?.size200,
      };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Harga Udang"),
        actions: [
          IconButton(
            icon: Icon(
              Icons.share,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
            onPressed: () {
              ShareHelper.shareUrl("https://app.jala.tech/shrimp_prices/$id");
            },
          ),
        ],
      ),
      body: SafeArea(
        child: BlocBuilder<HargaUdangDetailCubit, HargaUdangDetailState>(
          builder: (ctx, state) {
            return state.maybeWhen(
              loading: () => const Center(child: CircularProgressIndicator()),
              success: (data) {
                return SmartRefresher(
                  controller:
                      ctx.read<HargaUdangDetailCubit>().refreshController,
                  enablePullUp: false,
                  enablePullDown: true,
                  onRefresh: () => ctx
                      .read<HargaUdangDetailCubit>()
                      .loadData(id: id, regionId: regionId),
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(12),
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.surface,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data.region?.provinceName?.capitalize ?? "-",
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              Text(
                                data.region?.name?.capitalize ?? "-",
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium
                                    ?.copyWith(color: AppColors.textMenu),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 4),
                        Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 8,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.surface,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    data.date?.formatDate() ?? "-",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium
                                        ?.copyWith(color: AppColors.textMenu),
                                  ),
                                  const SizedBox(width: 12),
                                  VerifiedChip(
                                    isVerified: data.creator?.buyer ?? false,
                                  ),
                                ],
                              ),
                              const SizedBox(height: 4),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(360),
                                    child: CachedNetworkImage(
                                      imageUrl:
                                          "https://app.jala.tech/storage/${data.creator?.avatar}",
                                      width: 32,
                                      height: 32,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Supplier",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall
                                              ?.copyWith(
                                                  color: Theme.of(context)
                                                      .colorScheme
                                                      .onSecondaryContainer),
                                        ),
                                        Text(
                                          data.creator?.name ?? "-",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 4),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Kontak",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall
                                              ?.copyWith(
                                                color: AppColors.textOutline,
                                              ),
                                        ),
                                        Text(
                                          data.creator?.phone ?? "-",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 12),
                                  PrimaryButton(
                                    title: "Hubungi",
                                    onPressed: () {
                                      UrlLauncherHelper.launch(
                                        data.creator?.phone ?? "",
                                      );
                                    },
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16),
                              Text(
                                "Daftar Harga",
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              const SizedBox(height: 10),
                              ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: getPrices(data).length,
                                separatorBuilder: (_, index) =>
                                    const SizedBox(height: 8),
                                itemBuilder: (_, index) => Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        "Size ${getPrices(data).keys.toList()[index].toString()}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge,
                                      ),
                                    ),
                                    const SizedBox(width: 16),
                                    Expanded(
                                      flex: 7,
                                      child: Text(
                                        getPrices(data)[getPrices(data)
                                                    .keys
                                                    .toList()[index]]
                                                ?.toDouble()
                                                .formatCurrency(
                                                    symbol: "Rp ") ??
                                            "Rp. -",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 16),
                              Text(
                                "Catatan",
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              const SizedBox(height: 4),
                              Text(
                                data.remark?.toString() ??
                                    "Latest update at ${data.updatedAt?.formatDate(pattern: "dd MMMM yyyy HH:mm")}",
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              orElse: () => const Center(child: CircularProgressIndicator()),
            );
          },
        ),
      ),
    );
  }
}
