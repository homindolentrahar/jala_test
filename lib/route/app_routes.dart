import 'package:go_router/go_router.dart';
import 'package:jala_test/features/harga_udang/detail/presentation/harga_udang_detail_page.dart';
import 'package:jala_test/features/kabar_udang/detail/presentation/detail_kabar_udang_page.dart';
import 'package:jala_test/features/main/presentation/main_page.dart';
import 'package:jala_test/features/penyakit_udang/detail/presentation/detail_penyakit_udang_page.dart';

abstract class AppRoutes {
  static final GoRouter router = GoRouter(
    routes: [
      GoRoute(
        path: RoutePaths.initial,
        name: RoutePaths.initial,
        builder: (ctx, state) => MainPage.getPage(),
      ),
      GoRoute(
        path: "${RoutePaths.hargaUdang}/:id",
        name: RoutePaths.hargaUdang,
        builder: (ctx, state) => HargaUdangDetailPage.getPage(
          id: state.pathParameters['id'] ?? "",
          regionId: state.extra?.toString() ?? "",
        ),
      ),
      GoRoute(
        path: "${RoutePaths.kabarUdang}/:id",
        name: RoutePaths.kabarUdang,
        builder: (ctx, state) => DetailKabarUdangPage.getPage(
          int.tryParse(state.pathParameters['id'] ?? "0"),
        ),
      ),
      GoRoute(
        path: "${RoutePaths.penyakitUdang}/:id",
        name: RoutePaths.penyakitUdang,
        builder: (ctx, state) => DetailPenyakitUdangPage.getPage(
          int.tryParse(state.pathParameters['id'] ?? "0"),
        ),
      ),
    ],
  );
}

abstract class RoutePaths {
  static const String initial = "/";
  static const String hargaUdang = "/harga_udang";
  static const String kabarUdang = "/kabar_udang";
  static const String penyakitUdang = "/penyakit_udang";
}
