import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color primary = Color(0xFF1B77DF);
  static const Color primaryDark = Color(0xFF004492);
  static const Color background = Color(0xFFEAEFF5);
  static const Color backgroundAlt = Color(0xFFF5F6F7);
  static const Color outline = Color(0xFFE5E5E5);
  static const Color outlineAlt = Color(0xFFF6F6F6);
  static const Color verifiedBg = Color(0xFFFFF8E7);

  static const Color textTitle = Color(0xFF454646);
  static const Color textSubtitle = Color(0xFF737373);
  static const Color textSubtitleAlt = Color(0xFF859ED1);
  static const Color textPrimary = Color(0xFF004492);
  static const Color textMenu = Color(0xFF737373);
  static const Color textOutline = Color(0xFFA09E9E);
}
