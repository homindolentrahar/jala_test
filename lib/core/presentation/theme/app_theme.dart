import 'package:flutter/material.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';

abstract class AppTheme {
  static final lightTheme = ThemeData(
    useMaterial3: true,
    brightness: Brightness.light,
    fontFamily: 'Lato',
    appBarTheme: const AppBarTheme(
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      backgroundColor: AppColors.primary,
      titleTextStyle: TextStyle(
        fontFamily: "Lato",
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 18,
      ),
    ),
    primaryColor: AppColors.primary,
    colorScheme: const ColorScheme(
      brightness: Brightness.light,
      primary: AppColors.primary,
      onPrimary: Colors.white,
      secondary: AppColors.primaryDark,
      onSecondary: Colors.white,
      error: Colors.red,
      onError: Colors.white,
      surface: Colors.white,
      onBackground: AppColors.textTitle,
      background: AppColors.background,
      onSurface: AppColors.textTitle,
      onPrimaryContainer: AppColors.textPrimary,
      secondaryContainer: AppColors.verifiedBg,
      onSecondaryContainer: AppColors.textSubtitleAlt,
      outline: AppColors.outline,
      outlineVariant: AppColors.outlineAlt,
    ),
    textTheme: const TextTheme(
      displayLarge: TextStyle(
        color: AppColors.textTitle,
        fontSize: 22,
        fontWeight: FontWeight.w900,
      ),
      headlineLarge: TextStyle(
        color: AppColors.textTitle,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
      headlineMedium: TextStyle(
        color: AppColors.textTitle,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      headlineSmall: TextStyle(
        color: AppColors.textTitle,
        fontSize: 14,
        fontWeight: FontWeight.bold,
      ),
      bodyLarge: TextStyle(
        color: AppColors.textTitle,
        fontSize: 16,
      ),
      bodyMedium: TextStyle(
        color: AppColors.textTitle,
        fontSize: 14,
      ),
      bodySmall: TextStyle(
        color: AppColors.textTitle,
        fontSize: 12,
      ),
    ),
  );
}
