import 'package:pull_to_refresh/pull_to_refresh.dart';

mixin CubitBaseMixin {
  final RefreshController refreshController = RefreshController();

  void refreshCompleted() {
    if (refreshController.isRefresh) refreshController.refreshCompleted();
    if (refreshController.isLoading) refreshController.loadComplete();
  }
}
