import 'package:flutter/material.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';

class ColoredTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabController controller;
  final List<Widget> tabs;

  const ColoredTabBar({
    super.key,
    required this.controller,
    required this.tabs,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
      ),
      child: TabBar(
        controller: controller,
        tabs: tabs.map((e) => e).toList(),
        labelPadding: const EdgeInsets.symmetric(vertical: 16),
        labelStyle: Theme.of(context)
            .textTheme
            .headlineSmall
            ?.copyWith(color: Theme.of(context).primaryColor),
        unselectedLabelStyle: Theme.of(context)
            .textTheme
            .headlineSmall
            ?.copyWith(color: AppColors.textMenu),
        automaticIndicatorColorAdjustment: false,
        indicatorColor: Theme.of(context).primaryColor,
        indicatorWeight: 4,
        indicatorSize: TabBarIndicatorSize.tab,
        dividerColor: Theme.of(context).colorScheme.outlineVariant,
        dividerHeight: 4,
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(56);
}
