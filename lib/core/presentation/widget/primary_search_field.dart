import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';

class PrimarySearchField extends StatelessWidget {
  final TextEditingController? controller;
  final String hint;
  final ValueChanged<String?> onLocationSearch;
  final VoidCallback onLocationReset;

  const PrimarySearchField({
    super.key,
    required this.hint,
    required this.onLocationSearch,
    required this.onLocationReset,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: TextField(
            controller: controller,
            onChanged: onLocationSearch,
            onSubmitted: onLocationSearch,
            textInputAction: TextInputAction.search,
            decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
              filled: true,
              fillColor: AppColors.backgroundAlt,
              hintText: hint,
              hintStyle: Theme.of(context)
                  .textTheme
                  .bodyLarge
                  ?.copyWith(color: AppColors.textOutline),
              prefixIcon: SvgPicture.asset(
                "assets/icons/ic_search.svg",
                color: AppColors.textOutline,
              ),
              prefixIconConstraints: const BoxConstraints(minWidth: 48),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
                borderSide: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
                borderSide: BorderSide(
                  color: Theme.of(context).colorScheme.primary,
                  width: 1.3,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 12),
        GestureDetector(
          onTap: onLocationReset,
          child: SvgPicture.asset("assets/icons/ic_clear.svg"),
        ),
      ],
    );
  }
}
