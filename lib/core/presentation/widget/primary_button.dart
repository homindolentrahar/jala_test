import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String title;
  final bool isUppercase;
  final VoidCallback onPressed;

  const PrimaryButton({
    super.key,
    required this.title,
    this.isUppercase = false,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 0,
      focusElevation: 0,
      disabledElevation: 0,
      highlightElevation: 0,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: onPressed,
      child: Text(
        isUppercase ? title.toUpperCase() : title,
        style: Theme.of(context)
            .textTheme
            .headlineSmall
            ?.copyWith(color: Theme.of(context).colorScheme.onPrimary),
      ),
    );
  }
}
