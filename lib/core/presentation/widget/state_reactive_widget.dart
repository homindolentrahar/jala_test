import 'package:flutter/material.dart';
import 'package:jala_test/core/data/remote/data/base_status.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';

class StateReactiveWidget extends StatelessWidget {
  final BaseStatus status;
  final bool hasMorePage;
  final String errorMessage;
  final Widget child;
  final Widget? loadingWidget;
  final Widget? emptyWidget;

  const StateReactiveWidget({
    super.key,
    required this.status,
    this.hasMorePage = false,
    this.errorMessage = "",
    required this.child,
    this.loadingWidget,
    this.emptyWidget,
  });

  @override
  Widget build(BuildContext context) {
    if (status == BaseStatus.success || hasMorePage) {
      return child;
    } else if (status == BaseStatus.error && errorMessage.isNotEmpty) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.error,
            size: 32,
            color: Theme.of(context).colorScheme.error,
          ),
          const SizedBox(height: 8),
          Text(
            "Terjadi kesalahan!",
            style: Theme.of(context)
                .textTheme
                .headlineLarge
                ?.copyWith(color: Theme.of(context).colorScheme.error),
          ),
          const SizedBox(height: 4),
          Text(
            errorMessage,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ],
      );
    } else if (status == BaseStatus.empty) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Data kosong",
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const SizedBox(height: 4),
          Text(
            "Data tidak dapat ditampilkan karena kosong",
            style: Theme.of(context)
                .textTheme
                .bodyMedium
                ?.copyWith(color: AppColors.textSubtitle),
          ),
        ],
      );
    } else if (status == BaseStatus.loading) {
      return loadingWidget ?? const Center(child: CircularProgressIndicator());
    }

    return const SizedBox.shrink();
  }
}
