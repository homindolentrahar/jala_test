import 'package:flutter/material.dart';
import 'package:jala_test/core/presentation/theme/app_colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatelessWidget {
  final double width;
  final double height;
  final BorderRadius? radius;

  const ShimmerWidget({
    super.key,
    this.width = 64,
    this.height = 16,
    this.radius,
  });

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.outline,
      highlightColor: AppColors.backgroundAlt,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          borderRadius: radius ?? BorderRadius.circular(4),
        ),
      ),
    );
  }
}
