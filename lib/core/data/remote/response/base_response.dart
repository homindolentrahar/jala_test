import 'package:jala_test/core/data/remote/data/dto/links_dto.dart';
import 'package:jala_test/core/data/remote/data/dto/meta_dto.dart';
import 'package:jala_test/core/data/remote/data/response_parser.dart';

class BaseResponse {
  final LinksDto? links;
  final MetaDto? meta;

  BaseResponse({this.links, this.meta});

  // factory BaseResponse.fromJson(Map<String, dynamic> json) => BaseResponse(
  //       links: json['links'] == null ? null : LinksDto.fromJson(json['links']),
  //       meta: json['meta'] == null ? null : MetaDto.fromJson(json['meta']),
  //     );

  Map<String, dynamic> toJson() => {
        'links': links?.toJson(),
        'meta': meta?.toJson(),
      };
}

class ObjectResponse<T> extends BaseResponse {
  final T? data;

  ObjectResponse({
    required LinksDto? links,
    required MetaDto? meta,
    this.data,
  }) : super(links: links, meta: meta);

  factory ObjectResponse.fromJson(Map<String, dynamic> json) => ObjectResponse(
        data: json['data'] == null
            ? null
            : ResponseParser<T>().fromJson(
                json['data'],
              ),
        links: json['links'] == null ? null : LinksDto.fromJson(json['links']),
        meta: json['meta'] == null ? null : MetaDto.fromJson(json['meta']),
      );

  @override
  Map<String, dynamic> toJson() => {
        'data': data == null ? null : ResponseParser<T>().toJson(data as T),
        'links': links?.toJson(),
        'meta': meta?.toJson(),
      };
}

class ListResponse<T> extends BaseResponse {
  final List<T>? data;

  ListResponse({
    required LinksDto? links,
    required MetaDto? meta,
    this.data,
  }) : super(links: links, meta: meta);

  factory ListResponse.fromJson(Map<String, dynamic> json) => ListResponse(
        data: json['data'] == null
            ? null
            : List<T>.from(
                json['data'].map((x) => ResponseParser<T>().fromJson(x)),
              ),
        links: json['links'] == null ? null : LinksDto.fromJson(json['links']),
        meta: json['meta'] == null ? null : MetaDto.fromJson(json['meta']),
      );

  @override
  Map<String, dynamic> toJson() => {
        'data': data?.map((x) => ResponseParser<T>().toJson(x)).toList(),
        'links': links?.toJson(),
        'meta': meta?.toJson(),
      };
}
