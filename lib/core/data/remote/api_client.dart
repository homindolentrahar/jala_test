import 'dart:io';

import 'package:jala_test/core/data/remote/dio/api_dio.dart';
import 'package:jala_test/core/data/remote/response/base_response.dart';
import 'package:dio/dio.dart' as dio;
import 'package:jala_test/environment.dart';

enum RestMethod {
  get('GET'),
  post('POST'),
  put('PUT'),
  delete('DELETE'),
  patch('PATCH');

  final String title;

  const RestMethod(this.title);
}

class ApiClient {
  Future<Map<String, dynamic>?> requestData({
    required String endpoint,
    required RestMethod method,
    Map<String, dynamic> headers = const {},
    Map<String, dynamic> body = const {},
    Map<String, dynamic>? queryParams,
    bool isMultipart = false,
  }) async {
    final contentType = isMultipart ? 'multipart/form-data' : null;
    final Map<String, dynamic> newHeaders = {
      'Accept': 'application/json',
    };

    dynamic data = body;
    newHeaders.addAll(headers);

    if (!newHeaders.containsKey("Authorization")) {
      newHeaders['Authorization'] = Environment.bearerToken;
    }

    if (isMultipart) {
      for (MapEntry<String, dynamic> entry in body.entries) {
        if (entry.value is File) {
          final File file = body[entry.key] as File;
          body[entry.key] = dio.MultipartFile.fromFileSync(
            file.path,
            filename: file.path.split(Platform.pathSeparator).last,
          );
        }
      }

      data = dio.FormData.fromMap(body);
    }

    final client = await ApiDio.getDio();
    final options = dio.Options(
      method: method.title,
      headers: newHeaders,
      contentType: contentType,
    )
        .compose(
          client.options,
          endpoint,
          data: data,
          queryParameters: queryParams,
        )
        .copyWith(baseUrl: Environment.baseUrl);

    final result = await client.fetch<Map<String, dynamic>>(options);

    return result.data;
  }

  Future<ObjectResponse<T>> requestObject<T>({
    required String endpoint,
    required RestMethod method,
    Map<String, dynamic> headers = const {},
    Map<String, dynamic> body = const {},
    Map<String, dynamic>? queryParams,
    bool isMultipart = false,
    d,
  }) async {
    final result = await requestData(
      endpoint: endpoint,
      method: method,
      body: body,
      headers: headers,
      queryParams: queryParams,
      isMultipart: isMultipart,
    );

    return ObjectResponse<T>.fromJson(result ?? {});
  }

  Future<ListResponse<T>> requestList<T>({
    required String endpoint,
    required RestMethod method,
    Map<String, dynamic> headers = const {},
    Map<String, dynamic> body = const {},
    Map<String, dynamic>? queryParams,
    bool isMultipart = false,
  }) async {
    final result = await requestData(
      endpoint: endpoint,
      method: method,
      body: body,
      headers: headers,
      queryParams: queryParams,
      isMultipart: isMultipart,
    );

    return ListResponse<T>.fromJson(result ?? {});
  }
}
