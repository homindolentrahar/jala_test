import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jala_test/core/data/remote/dio/settings_dto.dart';
import 'package:jala_test/core/data/remote/dio/state_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/creator_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/harga_udang_dto.dart';
import 'package:jala_test/features/harga_udang/data/dto/region_dto.dart';
import 'package:jala_test/features/kabar_udang/data/dto/kabar_udang_dto.dart';
import 'package:jala_test/features/penyakit_udang/data/dto/penyakit_udang_dto.dart';

bool typeEqual<L, R>() => L == R;

bool typeEqualn<L, R>() => typeEqual<L, R>() || typeEqual<L?, R?>();

class ResponseParser<T> extends JsonConverter<T, Object> {
  @override
  T fromJson(Object json) {
    if (json is List && json.isEmpty) json = <String, dynamic>{};
    json = json as Map<String, dynamic>;

    if (typeEqualn<T, HargaUdangDto>()) {
      return HargaUdangDto.fromJson(json) as T;
    } else if (typeEqualn<T, CreatorDto>()) {
      return CreatorDto.fromJson(json) as T;
    } else if (typeEqualn<T, SettingsDto>()) {
      return SettingsDto.fromJson(json) as T;
    } else if (typeEqualn<T, StateDto>()) {
      return StateDto.fromJson(json) as T;
    } else if (typeEqualn<T, RegionDto>()) {
      return RegionDto.fromJson(json) as T;
    } else if (typeEqualn<T, KabarUdangDto>()) {
      return KabarUdangDto.fromJson(json) as T;
    } else if (typeEqualn<T, PenyakitUdangDto>()) {
      return PenyakitUdangDto.fromJson(json) as T;
    } else if (typeEqualn<T, dynamic>()) {
      return dynamic as T;
    }

    throw UnimplementedError("`$T` fromJson factory not implemented");
  }

  @override
  Object toJson(T object) {
    if (typeEqualn<T, HargaUdangDto>()) {
      return (object as HargaUdangDto).toJson();
    } else if (typeEqualn<T, CreatorDto>()) {
      return (object as CreatorDto).toJson();
    } else if (typeEqualn<T, SettingsDto>()) {
      return (object as SettingsDto).toJson();
    } else if (typeEqualn<T, StateDto>()) {
      return (object as StateDto).toJson();
    } else if (typeEqualn<T, RegionDto>()) {
      return (object as RegionDto).toJson();
    } else if (typeEqualn<T, KabarUdangDto>()) {
      return (object as KabarUdangDto).toJson();
    } else if (typeEqualn<T, PenyakitUdangDto>()) {
      return (object as PenyakitUdangDto).toJson();
    } else if (typeEqualn<T, dynamic>()) {
      return (object as dynamic);
    }

    throw UnimplementedError("`$T` fromJson factory not implemented");
  }
}
