class LinksDto {
  final String? first;
  final String? last;
  final dynamic prev;
  final String? next;

  LinksDto({
    this.first,
    this.last,
    this.prev,
    this.next,
  });

  factory LinksDto.fromJson(Map<String, dynamic> json) => LinksDto(
        first: json["first"],
        last: json["last"],
        prev: json["prev"],
        next: json["next"],
      );

  Map<String, dynamic> toJson() => {
        "first": first,
        "last": last,
        "prev": prev,
        "next": next,
      };
}
