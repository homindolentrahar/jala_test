import 'package:dio/dio.dart';
import 'package:jala_test/core/data/remote/dio/api_interceptor.dart';

abstract class ApiDio {
  static Future<Dio> getDio({
    Map<String, dynamic> headers = const {},
    int connectionTimeout = 30000,
    int receiveTimeout = 30000,
    InterceptorsWrapper? interceptor,
  }) async {
    final dio = Dio(
      BaseOptions(
        connectTimeout: Duration(milliseconds: connectionTimeout),
        receiveTimeout: Duration(milliseconds: receiveTimeout),
        headers: headers,
      ),
    );

    dio.interceptors.removeImplyContentTypeInterceptor();
    dio.interceptors.add(interceptor ?? ApiInterceptor());

    return dio;
  }
}
