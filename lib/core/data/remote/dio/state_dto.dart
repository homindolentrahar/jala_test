class StateDto {
  final DateTime? phoneVerificationExpiredAt;
  final dynamic mailjetId;
  final dynamic partner;
  final dynamic consultationTourComplete;
  final dynamic phoneVerificationOtpSent;
  final DateTime? phoneUpdatedAt;

  StateDto({
    this.phoneVerificationExpiredAt,
    this.mailjetId,
    this.partner,
    this.consultationTourComplete,
    this.phoneVerificationOtpSent,
    this.phoneUpdatedAt,
  });

  factory StateDto.fromJson(Map<String, dynamic> json) => StateDto(
        phoneVerificationExpiredAt:
            json["phone_verification_expired_at"] == null
                ? null
                : DateTime.parse(json["phone_verification_expired_at"]),
        mailjetId: json["mailjet_id"],
        partner: json["partner"],
        consultationTourComplete: json["consultation_tour_complete"],
        phoneVerificationOtpSent: json["phone_verification_otp_sent"],
        phoneUpdatedAt: json["phone_updated_at"] == null
            ? null
            : DateTime.parse(json["phone_updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "phone_verification_expired_at":
            phoneVerificationExpiredAt?.toIso8601String(),
        "mailjet_id": mailjetId,
        "partner": partner,
        "consultation_tour_complete": consultationTourComplete,
        "phone_verification_otp_sent": phoneVerificationOtpSent,
        "phone_updated_at": phoneUpdatedAt?.toIso8601String(),
      };
}
