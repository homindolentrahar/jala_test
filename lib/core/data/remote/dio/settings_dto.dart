class SettingsDto {
  final String? locale;

  SettingsDto({
    this.locale,
  });

  factory SettingsDto.fromJson(Map<String, dynamic> json) => SettingsDto(
        locale: json["locale"],
      );

  Map<String, dynamic> toJson() => {
        "locale": locale,
      };
}
