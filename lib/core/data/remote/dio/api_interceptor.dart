import 'dart:developer';
import 'package:dio/dio.dart';

class ApiInterceptor extends InterceptorsWrapper {
  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    log(
      "ERROR => ${err.requestOptions.uri}\nMessage: ${err.error.toString()}\nBody: ${err.message}\nResponse: ${err.response}\nEND ERROR",
    );

    return super.onError(err, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    var requestData = options.data;

    if (requestData is FormData) {
      requestData = requestData.fields;
    }

    log(
      "REQUEST => ${options.method.toUpperCase()} - ${options.uri}\nData: $requestData\nContent-Type: ${options.contentType}\nHeaders: ${options.headers}\nEND REQUEST",
    );

    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    log(
      "RESPONSE => ${response.statusCode} - ${response.requestOptions.uri}\nHeaders: ${response.headers['Authentication']}\nResponse: ${response.data}\nEND REQUEST",
    );

    return super.onResponse(response, handler);
  }
}
