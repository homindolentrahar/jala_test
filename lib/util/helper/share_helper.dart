import 'package:share_plus/share_plus.dart';

abstract class ShareHelper {
  static Future<void> shareUrl(String url) async {
    return Share.shareUri(Uri.parse(url));
  }
}
