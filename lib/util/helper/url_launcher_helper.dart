import 'package:url_launcher/url_launcher.dart';

abstract class UrlLauncherHelper {
  static Future<bool> launch(String url) async {
    final uri = Uri(
      scheme: 'tel',
      path: url,
    );

    return launchUrl(uri);
  }
}
