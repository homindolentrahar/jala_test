import 'package:intl/intl.dart';

extension DateTimeExt on DateTime {
  String formatDate({String pattern = "dd MMMM yyyy"}) =>
      DateFormat(pattern).format(this);
}
