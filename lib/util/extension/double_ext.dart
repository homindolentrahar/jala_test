import 'package:intl/intl.dart';

extension DoubleExt on double {
  String formatCurrency({
    String symbol = "IDR ",
    String locale = "id_ID",
    int decimalDigits = 0,
  }) =>
      NumberFormat.currency(
        symbol: symbol,
        decimalDigits: decimalDigits,
        locale: locale,
      ).format(this);
}
