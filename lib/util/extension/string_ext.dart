extension StringExt on String {
  String get capitalize => split(" ")
      .map((e) => "${e[0].toUpperCase()}${e.substring(1).toLowerCase()}")
      .join(" ");
}
